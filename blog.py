from flask import Flask, g, request, url_for, render_template, redirect
import sqlite3, time
from flask.ext.bootstrap import Bootstrap
from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import Required

class AddPost(Form):
    title = StringField('Title', validators=[Required()])
    text = TextAreaField('Text', validators=[Required()])
    submit = SubmitField('Submit')

#configuration
DATABASE = 'blog.db'
DEBUG = True
SECRET_KEY = 'fgjsldsdfwerqwi2342fvdsfg'
BOOTSTRAP_SERVE_LOCAL = True

app = Flask(__name__)
app.config.from_object(__name__)
bootstrap = Bootstrap(app)


def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

@app.route('/')
def index():
    cur = g.db.execute('select title, text, id, date from posts order by id desc')
    posts = [dict(title=row[0], text=row[1], post_id=row[2], date=row[3]) for row in cur.fetchall()]
    return render_template('index.html', posts=posts)

@app.route('/add', methods=['POST', 'GET'])
def add_post():
    form = AddPost()
    if form.validate_on_submit():
        g.db.execute('insert into posts (title, text, date) values (?, ?, ?)',
                [form.title.data, form.text.data, time.strftime("%d.%m.%Y %H:%M:%S")])
        g.db.commit()
        return redirect(url_for('index'))
    return render_template('add.html', form=form)

@app.route('/post/<int:post_id>')
def show_post(post_id):
    cur = g.db.execute('select title, text, date from posts where id=?', (post_id,))
    post = [dict(title=row[0], text=row[1], date=row[2]) for row in cur.fetchall()]
    return render_template('post.html', post=post[0])

if __name__ == '__main__':
    app.run()
